<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Transaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('transaksi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_id')->unsigned();
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->string('kamar_id')->unsigned();
            $table->foreign('kamar_id')->references('id')->on('kamars');
            $table->string('tanggal_checkIn');
            $table->string('tanggal_checkOut');
            $table->decimal('tarif');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
