<?php

namespace App\Http\Controllers;

use App\Kamar;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class BookingKamarController extends Controller
{
    function getData()
    {
								    

    	$roomlist = BookingKamar::get();				

    	return response()->json($roomlist, 200);	
    }

    function bookingRoom(Request $request)
    {
          
          DB::beginTransaction();
          try{
                $this->validate($request,
                    ['customer_id'=>'required','kamar_id'=>'required']
                    );

                  

            $bok = new BookingKamar;
            $bok->customer_id = $request->customer_id;
            $bok->kamar_id =$request->kamar_id;
            $bok->tanggal_checkIn = date("Y-m-d h:i:s");
            $bok->save();

            $room = Kamar::find($request->id);
            $room->status_kamar = true;
            $room->save();



     

            DB::commit();

        return "kamar telah di booking";
          
          }  
          catch(\Exception $e)
    	{
    		DB::rollback();
    		return "Terjadi Error";			
    	}
    }


}
