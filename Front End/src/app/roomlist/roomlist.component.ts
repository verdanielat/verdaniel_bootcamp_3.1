import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-roomlist',
  templateUrl: './roomlist.component.html',
  styleUrls: ['./roomlist.component.css']
})
export class RoomlistComponent implements OnInit {


  constructor() { }

  ngOnInit() {
  }

 roomList : Object[] = [
    {"id":"1", "type":"Room 1", "price":200, "img":"/assets/img/hotel.jpg", "status_kamar":1,"desc":"green room"},
    {"id":"2", "type":"Room 2", "price":300, "img":"/assets/img/hotel.jpg", "status_kamar":1,"desc":"brown room"},
    {"id":"3", "type":"Room 3", "price":700, "img":"/assets/img/hotel.jpg", "status_kamar":0,"desc":"purple room"}
  ];

  book(index){

    this.roomList[index]["status"] = 1;

  }
  
}
